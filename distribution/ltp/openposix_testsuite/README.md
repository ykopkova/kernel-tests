# LTP Open POSIX Testsuite  
The POSIX Test Suite is an open source test suite with the goal of
performing conformance, functional, and stress testing of the IEEE
1003.1-2001 System Interfaces specification.

## How to run it
Please refer to the top-level README.md for common dependencies. For a complete detail, see PURPOSE file.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
